package net.kriemhild;

import java.util.Map;

public class PickingProcess {
    public PickingProcess(String id) {
        this.id = id;
    }

    enum State {
        Idle,
        Collecting,
        Controlling,
        Error,
        Completed
    }

    public String id;
    public String collectId;
    public String controlId;

    public State state = State.Idle;

    public Map<String, Integer> articles;
}
