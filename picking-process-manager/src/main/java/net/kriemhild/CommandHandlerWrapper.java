package net.kriemhild;

import warehouse.Command;
import warehouse.CommandHandlerGrpc;

public class CommandHandlerWrapper {
    private CommandHandlerGrpc.CommandHandlerBlockingStub stub;

    public CommandHandlerWrapper(CommandHandlerGrpc.CommandHandlerBlockingStub stub) {
        this.stub = stub;
    }

    void handle(Command command){
        stub.handle(command);
    }
}
