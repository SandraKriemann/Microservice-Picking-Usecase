package net.kriemhild;

import com.google.protobuf.Any;
import com.google.protobuf.InvalidProtocolBufferException;
import vlek.Event;
import warehouse.*;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.UUID;

public class PickingProcessManager {
    private PickingProcessStore store;
    private CommandHandlerWrapper collect;
    private CommandHandlerWrapper control;
    private CommandHandlerWrapper picking;

    public PickingProcessManager(
            PickingProcessStore store,
            CommandHandlerWrapper collect,
            CommandHandlerWrapper control,
            CommandHandlerWrapper picking) {
        this.store = store;
        this.collect = collect;
        this.control = control;
        this.picking = picking;
    }

    public void handle(Event event) throws InvalidProtocolBufferException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method handler = PickingProcessManager.class.getDeclaredMethod("handle" + event.getEventName(), Event.class);
        handler.invoke(this, event);
    }

    private void handlePickingCreated(Event event) throws InvalidProtocolBufferException {
        PickingProcess process = store.getById(event.getId());
        PickingCreatedPayload payload = event.getData().unpack(PickingCreatedPayload.class);

        String collectId = UUID.randomUUID().toString();
        Map<String, Integer> articles = payload.getArticlesMap();

        Command createCollect = Command.newBuilder()
                .setId(collectId)
                .setActor(event.getActor())
                .setCommandName("CreateCollect")
                .setData(Any.pack(CreateCollectPayload.newBuilder()
                        .setPicking(event.getId())
                        .putAllArticles(articles)
                        .build()))
                .setProcessId(UUID.randomUUID().toString())
                .build();

        this.collect.handle(createCollect);

        process.collectId = collectId;
        process.state = PickingProcess.State.Collecting;
        process.articles = articles;

        store.save(process);
    }

    private void handleCollectCompleted(Event event) {
        PickingProcess process = store.getByCollectId(event.getId());

        if (process == null || process.state != PickingProcess.State.Collecting) {
            System.out.println(String.format("Process Error with %s", event.toString()));
            return;
        }

        String controlId = UUID.randomUUID().toString();

        Command createControl = Command.newBuilder()
                .setId(controlId)
                .setActor(event.getActor())
                .setCommandName("CreateControl")
                .setData(Any.pack(CreateControlPayload.newBuilder()
                        .setPicking(process.id)
                        .putAllArticles(process.articles)
                        .build()))
                .setProcessId(UUID.randomUUID().toString())
                .build();

        control.handle(createControl);

        process.controlId = controlId;
        process.state = PickingProcess.State.Controlling;

        store.save(process);
    }

    private void handleControlCompleted(Event event) {
        PickingProcess process = store.getByControlId(event.getId());

        if (process == null || process.state != PickingProcess.State.Controlling) {
            System.out.println(String.format("Process Error with %s", event.toString()));
            return;
        }

        Command completePicking = Command.newBuilder()
                .setId(process.id)
                .setActor(event.getActor())
                .setCommandName("CompletePicking")
                .setProcessId(UUID.randomUUID().toString())
                .build();

        picking.handle(completePicking);

        process.state = PickingProcess.State.Completed;

        store.save(process);
    }
}
