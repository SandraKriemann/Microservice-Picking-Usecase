package net.kriemhild;

public interface PickingProcessStore {
    PickingProcess getById(String id);

    PickingProcess getByCollectId(String id);

    PickingProcess getByControlId(String id);

    void save(PickingProcess process);
}
