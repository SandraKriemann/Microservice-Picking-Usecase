##
# BUILD STAGE
##
FROM maven:alpine AS build

WORKDIR /app

# Install glibc dependency for protoc
# Hack to also restore protoc artifacts with dependency:go-offline
RUN apk --no-cache add ca-certificates wget &&  \
    wget -q -O /etc/apk/keys/sgerrand.rsa.pub https://alpine-pkgs.sgerrand.com/sgerrand.rsa.pub && \
    wget https://github.com/sgerrand/alpine-pkg-glibc/releases/download/2.28-r0/glibc-2.28-r0.apk && \
    apk add glibc-2.28-r0.apk && \
    mkdir -p src/main/proto && echo 'syntax = "proto3";' > src/main/proto/tmp.proto

# Install dependencies
# INFO: Custom maven repo folder, because Docker clears ~/.m2 in each step
# INFO: verify and dependency:go-offline, otherwise there are missing deps for install
COPY ./pom.xml .
RUN mvn -B -Dmaven.repo.local=/maven package dependency:go-offline -U -Pquick,test -DskipTests

# Build artifact
# INFO: Custom maven repo folder, because Docker clears ~/.m2 in each step
COPY ./src ./src
# Can't do an offline build, so missing deps are downloaded each time we build.
# Go-offline is not able to download all dependencies from pom due to a bug in Maven.
# See also: https://issues.apache.org/jira/browse/MDEP-516
RUN mvn -B -Dmaven.repo.local=/maven package

##
# RELEASE STAGE
##
FROM openjdk:alpine AS release

COPY --from=build /app/target/*dependencies.jar /app.jar

CMD java -jar /app.jar
