import pbjs from 'protobufjs'
import stan from 'node-nats-streaming'
import { Collection, Picking, Control } from './db'

require('./pbjs-wrapper')(pbjs)

const protos = pbjs.loadSync([
  __dirname + '/proto/event.proto',
  __dirname + '/proto/picking-events.proto',
  __dirname + '/proto/collect-events.proto',
  __dirname + '/proto/control-events.proto'
])

const Event = protos.root.lookupType('vlek.Event')

let client = stan.connect(
  'test-cluster',
  process.env.STAN_CLIENT || 'warehouse-projector',
  process.env.NATS_URL || 'nats://localhost:4222')

function extractEventPayload (msg) {
  let data = msg.getRawData()

  if (!Event.verify(data)) {
    console.log('fail')
  }

  let event = Event.toObject(Event.decode(data))
  console.log(`event: ${event.eventName} (${event.id})`)
  return event
}

client.on('connect', () => {
  let opts = client.subscriptionOptions()
    .setDeliverAllAvailable()
    .setDurableName('warehouse-projector')

  client.subscribe('PickingCreated').on('message', async (msg) => {
    try {
      let event = extractEventPayload(msg)

      let articles = Object
        .keys(event.data.articles)
        .reduce((list, key) => {
          list.push({id: key, amount: event.data.articles[key]})
          return list
        }, [])

      await new Picking({
        id: event.id,
        articles
      }).save()
    } catch (err) {
      console.log(err)
    }
  })

  client.subscribe('CollectCreated').on('message', async (msg) => {
    try {
      let event = extractEventPayload(msg)

      let articles = Object
        .keys(event.data.articles)
        .reduce((list, key) => {
          list.push({id: key, amount: event.data.articles[key]})
          return list
        }, [])

      await new Collection({
        id: event.id,
        articles
      }).save()
    } catch (err) {
      console.log(err)
    }
  })

  client.subscribe('ControlCreated').on('message', async (msg) => {
    try {
      let event = extractEventPayload(msg)

      let articles = Object
        .keys(event.data.articles)
        .reduce((list, key) => {
          list.push({id: key, amount: event.data.articles[key]})
          return list
        }, [])

      await new Control({
        id: event.id,
        articles
      }).save()
    } catch (err) {
      console.log(err)
    }
  })

  client.subscribe('CollectorAssigned').on('message', async (msg) => {
    try {
      let event = extractEventPayload(msg)
      await Collection.update({id: event.id}, {collector: event.data.user})
    } catch (err) {
      console.log(err)
    }
  })

  client.subscribe('ControllerAssigned').on('message', async (msg) => {
    try {
      let event = extractEventPayload(msg)
      await Control.update({id: event.id}, {controller: event.data.user})
    } catch (err) {
      console.log(err)
    }
  })

})

client.on('error', (err) => {
  console.log(err)
  process.exit(1)
})

client.on('close', () => console.log('close') & process.exit())

process.on('SIGINT', () => console.log('shutdown') & client.close())

setTimeout(() => {}, -1)
