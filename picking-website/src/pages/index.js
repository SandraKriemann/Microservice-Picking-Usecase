import Layout from '../components/MyLayout.js'
import { compose, graphql } from 'react-apollo'
import gql from 'graphql-tag'
import withData from '../lib/withData'

const Index = ({data: {pickings, collections}, startPicking, startCollect}) => (
  <Layout>
    <table className="rootTable">
      <tbody>
      <tr>
        <td>
          <h1>Pickings</h1>
          <ul>
            {pickings && pickings.map(p =>
              <li key={p.id}>
                <p>{`Picking: ${p.id}`}</p>
                <ul>
                  {p.articles && p.articles.map(a => <li key={a.id}>{`${a.id}: ${a.amount}`}</li>)}
                </ul>
              </li>
            )}
          </ul>
          <button onClick={() => startPicking()}>Start next Picking</button>
        </td>


        <td>
          <h1>Collections</h1>
          <ul>
            {collections && collections.map(c =>
              <li key={c.id}>
                <p>{`Einsammlungen: ${c.id}`}</p>
                <ul>
                  {c.articles && c.articles.map(ar => <li key={ar.id}>{`${ar.id}: ${ar.amount}`}</li>)}
                </ul>
              </li>
            )}
          </ul>
          <button onClick={() => startCollection()}>Start next Control</button>
        </td>
      </tr>
      </tbody>

    </table>
    <style jsx>{`
     .rootTable {
      width: 100%
     }
   `}</style>
  </Layout>
)

const PickingQuery = gql`
  {
    pickings {id, articles {id, amount}}
    
    collections {id, articles {id, amount}}
  }
`

const PickingMutation = gql`
  mutation {
    startPicking
  }
`


const CollectMutation = gql`
  mutation {
    startCollection
  }
`

const IndexWithData = compose(
  graphql(PickingQuery),
  graphql(PickingMutation, {
    props: ({mutate}) => ({startPicking: mutate})
  }),
  graphql(CollectMutation, {
    props: ({mutate}) => ({startCollect: mutate})
  })
)(Index)

export default withData(IndexWithData)
