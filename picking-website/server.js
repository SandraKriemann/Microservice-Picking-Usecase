const micro = require('micro')
const {parse} = require('url')
const next = require('next')

const port = process.env.PORT || 3000
const dev = process.env.NODE_ENV !== 'production'
const app = next({dev, dir: './src'})
const handle = app.getRequestHandler()

const userid = (process.env && process.env.X_AUTHENTICATED_USERID) || '987d5f61-c85c-4900-8674-9361df40f652'
const apiEndpoint = (process.env && process.env.API_ENDPOINT) || 'http://localhost:3030'

const server = micro(async (req, res) => {
  if (req.url.match(/\/api\/graphql$/)) {
    const response = await fetch(apiEndpoint, {
      method: 'POST',
      headers: {
        'x-authenticated-userid': userid,
        ...req.headers
      },
      body: await micro.buffer(req)
    })

    return await response.buffer()
  }

  const parsedUrl = parse(req.url, true)
  return handle(req, res, parsedUrl)
})

app.prepare().then(() => {
  server.listen(port, err => {
    if (err) throw err
    console.log(`> Ready on http://localhost:${port}`)
  })
})