import mongoose from 'mongoose'

mongoose.connect(
    process.env.MONGO_URL || 'mongodb://localhost/test',
    {useMongoClient: true, promiseLibrary: global.Promise}
)

export const Article = mongoose.model('Article', new mongoose.Schema({}, {strict: false}))

export const User = mongoose.model('User', new mongoose.Schema({}, {strict: false}))
