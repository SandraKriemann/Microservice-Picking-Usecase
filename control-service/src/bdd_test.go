package main

import (
	. "github.com/onsi/gomega"
	"github.com/nu7hatch/gouuid"
	. "./proto"
	. "./proto/warehouse"
	"github.com/gogo/protobuf/types"
	"github.com/gogo/protobuf/proto"
	. "github.com/onsi/gomega/gstruct"
)

type AggregateHandler interface {
	When(cmd string, payload proto.Message) AggregateAssertation
}

type AggregateAssertation interface {
	// No event should spawn. The aggregate should return an error.
	ThenFail()

	// The event name and payload the aggregate should spawn.
	Then(eventName string, fields Fields)

	// The event name and payload the aggregate should spawn.
	ThenMany(events map[string]Fields)
}

type AggregateTest struct {
	aggregate Aggregate

	err    error
	events []*Event
}

func Given(events ...*Event) AggregateHandler {
	idd, _ := uuid.NewV4()

	id := idd.String()

	for i, event := range events {
		event.Id = id
		event.Version = int32(i + 1)
	}

	return &AggregateTest{
		aggregate: NewControlAggregate(id, events),
	}
}

func (a *AggregateTest) When(commandName string, payload proto.Message) AggregateAssertation {
	var data *types.Any

	if payload != nil {
		var err error
		data, err = types.MarshalAny(payload)

		if err != nil {
			panic(err)
		}
	} else {
		data = nil
	}

	cmd := &Command{
		CommandName: commandName,
		Data:        data,
	}

	events, err := a.aggregate.Handle(cmd)

	a.err = err
	a.events = events

	return a
}

func (t *AggregateTest) Then(eventName string, fields Fields) {
	Expect(t.err).To(BeNil())
	Expect(len(t.events)).To(Equal(1))

	var dataMatcher OmegaMatcher
	if fields != nil {
		dataMatcher = UnpackTo(PointTo(MatchAllFields(fields)))
	} else {
		dataMatcher = BeNil()
	}

	Expect(*t.events[0]).To(MatchFields(IgnoreExtras, Fields{
		"Id":        Equal(t.aggregate.Id()),
		"Version":   BeNumerically("==", t.aggregate.Version()+1),
		"EventName": Equal(eventName),
		"Data":      dataMatcher,
	}))
}

func (t *AggregateTest) ThenMany(events map[string]Fields) {
	Expect(t.err).To(BeNil())
	Expect(len(t.events)).To(Equal(len(events)))

	for i, event := range t.events {
		fields := events[event.EventName]
		var dataMatcher OmegaMatcher
		if fields != nil {
			dataMatcher = UnpackTo(PointTo(MatchAllFields(fields)))
		} else {
			dataMatcher = BeNil()
		}

		Expect(*event).To(MatchFields(IgnoreExtras, Fields{
			"Id":        Equal(t.aggregate.Id()),
			"Version":   BeNumerically("==", t.aggregate.Version()+i+1),
			"EventName": Equal(event.EventName),
			"Data":      dataMatcher,
		}))
	}
}

func (t *AggregateTest) ThenFail() {
	Expect(t.err).NotTo(BeNil())
}
