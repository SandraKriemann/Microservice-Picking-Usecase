package main

import (
	"os"
	"regexp"
	"log"
)

// Get the Env variable with given key or returns def value if not present
func GetEnv(key, def string) string{
	v := os.Getenv(key)
	if v == "" {
		return def
	}

	return v
}

// Checks if url is a valid gRPC Urls otherwise it exit with 1
func CheckgRPCUrl(url string) {
	if ok, _ := regexp.MatchString("^(.*):[0-9]{1,5}$", url); !ok {
		log.Fatal("Error gRPC urls has to contain a PORT")
		os.Exit(1)
	}
}
