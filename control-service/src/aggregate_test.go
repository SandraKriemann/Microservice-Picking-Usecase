package main

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"testing"
	"github.com/nu7hatch/gouuid"
	. "./proto"
	. "./proto/warehouse"
	"github.com/gogo/protobuf/types"
	. "github.com/onsi/gomega/gstruct"
)

func TestEventAggregate(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Control Aggregate Suite")
}

var _ = Describe("Control Aggregate", func() {
	Describe("Empty Aggregate", func() {

		It("should has version 0", func() {
			idd, _ := uuid.NewV4()
			aggregate := NewControlAggregate(idd.String(), []*Event{})

			Expect(aggregate).NotTo(BeNil())

			Expect(aggregate.Id()).To(BeUUID())
			Expect(aggregate.Version()).To(Equal(0))
		})

		It("sources ControlCreated event", func() {
			idd, _ := uuid.NewV4()

			payload := &CreateControlPayload{
				Picking: idd.String(),
				Articles: map[string]int32{
					"Medicine1": 3,
				},
			}

			Given().
				When(CREATE_CONTROL, payload).
				Then(CONTROL_CREATED, Fields{
				"Picking":  Equal(payload.Picking),
				"Articles": Equal(payload.Articles),
			})
		})

		It("fails if warehouse id is missing", func() {
			payload := &CreateControlPayload{
				Articles: map[string]int32{
					"name": 1,
				},
			}

			Given().
				When(CREATE_CONTROL, payload).
				ThenFail()
		})

		It("fails for Delete Command", func() {
			Given().
				When(DELETE_CONTROL, nil).
				ThenFail()
		})

	})

	Describe("with created and assigned control aggregate", func() {
		var (
			controlCreated    *Event
			controllerAssigned *Event
			demoArticle       string
			demoArticle1      string
		)

		BeforeEach(func() {
			idd, _ := uuid.NewV4()

			demoArticle = idd.String()
			demoArticle1 = idd.String()
			createPayload, _ := types.MarshalAny(&ControlCreatedPayload{
				Picking:  idd.String(),
				Articles: map[string]int32{demoArticle: 1, demoArticle1: 4},
			})
			controlCreated = &Event{
				EventName: CONTROL_CREATED,
				Data:      createPayload,
			}

			controllerAssignedPayload, _ := types.MarshalAny(&ControllerAssignedPayload{
				User: idd.String(),
			})
			controllerAssigned = &Event{
				EventName: CONTROLLER_ASSIGNED,
				Data:      controllerAssignedPayload,
			}
		})

		It("can be deleted", func() {
			Given(controlCreated, controllerAssigned).
				When(DELETE_CONTROL, nil).
				Then(CONTROL_DELETED, nil)
		})

		It("can control an article", func() {
			Given(controlCreated, controllerAssigned).
				When(CONTROL_ARTICLE, &ControlArticlePayload{demoArticle, 1}).
				Then(ARTICLE_CONTROLLED, Fields{
				"Article": Equal(demoArticle),
				"Amount": Equal (int32(1)),
			})
		})

		It("fail on unknown article", func() {
			idd, _ := uuid.NewV4()

			Given(controlCreated, controllerAssigned).
				When(CONTROL_ARTICLE, &ControlArticlePayload{idd.String(), 1}).
				ThenFail()
		})
	})

	Describe("with created and unassigned control aggregate", func() {
		var (
			controlCreated *Event
			demoArticle    string
		)

		BeforeEach(func() {
			idd, _ := uuid.NewV4()

			demoArticle = idd.String()
			createPayload, _ := types.MarshalAny(&ControlCreatedPayload{
				Picking:  idd.String(),
				Articles: map[string]int32{demoArticle: 1},
			})
			controlCreated = &Event{
				EventName: CONTROL_CREATED,
				Data:      createPayload,
			}
		})

		It("can be deleted", func() {
			Given(controlCreated).
				When(DELETE_CONTROL, nil).
				Then(CONTROL_DELETED, nil)
		})

		It("no article can be controlled", func() {
			Given(controlCreated).
				When(CONTROL_ARTICLE, nil).
				ThenFail()
		})
	})

	Describe("with deleted event", func() {
		var (
			controlCreated *Event
			controlDeleted *Event
			demoArticle    string
		)

		BeforeEach(func() {
			idd, _ := uuid.NewV4()

			demoArticle = idd.String()
			createPayload, _ := types.MarshalAny(&ControlCreatedPayload{
				Picking:  idd.String(),
				Articles: map[string]int32{demoArticle: 1},
			})
			controlCreated = &Event{
				EventName: CONTROL_CREATED,
				Data:      createPayload,
			}

			controlDeleted = &Event{
				EventName: CONTROL_DELETED,
			}
		})

		It("can not be deleted again", func() {
			Given(controlCreated, controlDeleted).
				When(DELETE_CONTROL, nil).
				ThenFail()
		})
	})

	Describe("controls articles", func() {
		var (
			controlCreated     *Event
			controllerAssigned *Event
			articleControlled  *Event
			demoArticle        string
			demoArticle1       string
		)

		BeforeEach(func() {
			idd, _ := uuid.NewV4()
			demoArticle = idd.String()
			demoArticle1 = idd.String()
			createPayload, _ := types.MarshalAny(&ControlCreatedPayload{
				Picking:  idd.String(),
				Articles: map[string]int32{demoArticle: 1, demoArticle1: 4},
			})
			controlCreated = &Event{
				EventName: CONTROL_CREATED,
				Data:      createPayload,
			}

			controllerAssignedPayload, _ := types.MarshalAny(&ControllerAssignedPayload{
				User: idd.String(),
			})
			controllerAssigned = &Event{
				EventName: CONTROLLER_ASSIGNED,
				Data:      controllerAssignedPayload,
			}

			articleControlledPayload, _ := types.MarshalAny(&ArticleControlledPayload{
				Article: demoArticle,
			})

			articleControlled = &Event{
				EventName: ARTICLE_CONTROLLED,
				Data:      articleControlledPayload,
			}

		})

		It("can control an article", func() {
			Given(controlCreated, controllerAssigned).
				When(CONTROL_ARTICLE, &ControlArticlePayload{demoArticle, 1}).
				Then(ARTICLE_CONTROLLED, Fields{
				"Article": Equal(demoArticle),
				"Amount": Equal(int32(1)),
			})
		})

		It("can control another article and completes control", func() {
			Given(controlCreated, controllerAssigned, articleControlled).
				When(CONTROL_ARTICLE, &ControlArticlePayload{demoArticle1, 4}).
				ThenMany(map[string]Fields{
				ARTICLE_CONTROLLED: {
					"Article": Equal(demoArticle1),
					"Amount": Equal(int32(4)),
				},
				CONTROL_COMPLETED: nil,
			})
		})

		It("fails on unknown article", func() {
			idd, _ := uuid.NewV4()

			Given(controlCreated, controllerAssigned).
				When(CONTROL_ARTICLE, &ControlArticlePayload{idd.String(), 1}).
				ThenFail()
		})
	})
})
