const mongoose = require('mongoose')

mongoose.connect(
  process.env.MONGO_URL || 'mongodb://localhost/test',
  {useMongoClient: true, promiseLibrary: global.Promise}
)

const Picking = mongoose.model('Picking', new mongoose.Schema({}, {strict: false}))

const Collection = mongoose.model('Collection', new mongoose.Schema({}, {strict: false}))

const Control = mongoose.model('Control', new mongoose.Schema({}, {strict: false}))

module.exports = {Picking, Collection, Control}