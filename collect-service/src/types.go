package main

// ******************
// *   AGGREGATES   *
// ******************

type CollectAggregate struct {
	id      string
	version int
	Deleted bool

	Picking string
	User string

	Articles map[string]int32
	CollectedArticles map[string]bool
}

func (a *CollectAggregate) Id() string   { return a.id }
func (a *CollectAggregate) Version() int { return a.version }

// ******************
// *	COMMANDS	*
// ******************

const CREATE_COLLECT = "COLLECT.CREATE_COLLECT"

const DELETE_COLLECT = "COLLECT.DELETE_COLLECT"

const COLLECT_ARTICLE = "COLLECT.COLLECT_ARTICLE"

const ASSIGN_COLLECTOR = "COLLECT.ASSIGN_COLLECTOR"

// ******************
// *     EVENTS     *
// ******************

const COLLECT_CREATED = "COLLECT.COLLECT_CREATED"

const COLLECT_DELETED = "COLLECT.COLLECT_DELETED"

const ARTICLE_COLLECTED = "COLLECT.ARTICLE_COLLECTED"

const COLLECTOR_ASSIGNED = "COLLECT.COLLECTOR_ASSIGNED"

const COLLECT_COMPLETED = "COLLECT.COLLECT_COMPLETED"
