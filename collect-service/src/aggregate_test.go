package main

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"testing"
	"github.com/nu7hatch/gouuid"
	. "./proto"
	. "./proto/warehouse"
	"github.com/gogo/protobuf/types"
	. "github.com/onsi/gomega/gstruct"
)

func TestEventAggregate(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Collect Aggregate Suite")
}

var _ = Describe("Collect Aggregate", func() {
	Describe("Empty Aggregate", func() {

		It("should has version 0", func() {
			idd, _ := uuid.NewV4()
			aggregate := NewCollectAggregate(idd.String(), []*Event{})

			Expect(aggregate).NotTo(BeNil())

			Expect(aggregate.Id()).To(BeUUID())
			Expect(aggregate.Version()).To(Equal(0))
		})

		It("sources CollectCreated event", func() {
			idd, _ := uuid.NewV4()

			payload := &CreateCollectPayload{
				Picking: idd.String(),
				Articles: map[string]int32{
					"Medicine1": 3,
				},
			}

			Given().
				When(CREATE_COLLECT, payload).
				Then(COLLECT_CREATED, Fields{
				"Picking":  Equal(payload.Picking),
				"Articles": Equal(payload.Articles),
			})
		})

		It("fails if warehouse id is missing", func() {
			payload := &CreateCollectPayload{
				Articles: map[string]int32{
					"name": 1,
				},
			}

			Given().
				When(CREATE_COLLECT, payload).
				ThenFail()
		})

		It("fails for Delete Command", func() {
			Given().
				When(DELETE_COLLECT, nil).
				ThenFail()
		})

	})

	Describe("with created and assigned collect aggregate", func() {
		var (
			collectCreated    *Event
			collectorAssigned *Event
			demoArticle       string
			demoArticle1      string
		)

		BeforeEach(func() {
			idd, _ := uuid.NewV4()
			demoArticle = idd.String()
			demoArticle1 = idd.String()
			createPayload, _ := types.MarshalAny(&CollectCreatedPayload{
				Picking:  idd.String(),
				Articles: map[string]int32{demoArticle: 1, demoArticle1: 4},
			})
			collectCreated = &Event{
				EventName: COLLECT_CREATED,
				Data:      createPayload,
			}

			collectorAssignedPayload, _ := types.MarshalAny(&CollectorAssignedPayload{
				User: idd.String(),
			})
			collectorAssigned = &Event{
				EventName: COLLECTOR_ASSIGNED,
				Data:      collectorAssignedPayload,
			}
		})

		It("can be deleted", func() {
			Given(collectCreated, collectorAssigned).
				When(DELETE_COLLECT, nil).
				Then(COLLECT_DELETED, nil)
		})

		It("can collect an article", func() {
			Given(collectCreated, collectorAssigned).
				When(COLLECT_ARTICLE, &CollectArticlePayload{demoArticle}).
				Then(ARTICLE_COLLECTED, Fields{
				"Article": Equal(demoArticle),
			})
		})

		It("fail on unknown article", func() {
			idd, _ := uuid.NewV4()
			Given(collectCreated, collectorAssigned).
				When(COLLECT_ARTICLE, &CollectArticlePayload{idd.String()}).
				ThenFail()
		})
	})

	Describe("with created and unassigned collect aggregate", func() {
		var (
			collectCreated *Event
			demoArticle    string
		)

		BeforeEach(func() {
			idd, _ := uuid.NewV4()
			demoArticle = idd.String()
			createPayload, _ := types.MarshalAny(&CollectCreatedPayload{
				Picking:  idd.String(),
				Articles: map[string]int32{demoArticle: 1},
			})
			collectCreated = &Event{
				EventName: COLLECT_CREATED,
				Data:      createPayload,
			}
		})

		It("can be deleted", func() {
			Given(collectCreated).
				When(DELETE_COLLECT, nil).
				Then(COLLECT_DELETED, nil)
		})

		It("no article can be collected", func() {
			Given(collectCreated).
				When(COLLECT_ARTICLE, nil).
				ThenFail()
		})
	})

	Describe("with deleted event", func() {
		var (
			collectCreated *Event
			collectDeleted *Event
			demoArticle    string
		)

		BeforeEach(func() {
			idd, _ := uuid.NewV4()
			demoArticle = idd.String()
			createPayload, _ := types.MarshalAny(&CollectCreatedPayload{
				Picking:  idd.String(),
				Articles: map[string]int32{demoArticle: 1},
			})
			collectCreated = &Event{
				EventName: COLLECT_CREATED,
				Data:      createPayload,
			}

			collectDeleted = &Event{
				EventName: COLLECT_DELETED,
			}
		})

		It("can not be deleted again", func() {
			Given(collectCreated, collectDeleted).
				When(DELETE_COLLECT, nil).
				ThenFail()
		})
	})

	Describe("collects articles", func() {
		var (
			collectCreated    *Event
			collectorAssigned *Event
			articleCollected  *Event
			demoArticle       string
			demoArticle1      string
		)

		BeforeEach(func() {
			idd, _ := uuid.NewV4()

			demoArticle = idd.String()
			demoArticle1 = idd.String()
			createPayload, _ := types.MarshalAny(&CollectCreatedPayload{
				Picking:  idd.String(),
				Articles: map[string]int32{demoArticle: 1, demoArticle1: 4},
			})
			collectCreated = &Event{
				EventName: COLLECT_CREATED,
				Data:      createPayload,
			}

			collectorAssignedPayload, _ := types.MarshalAny(&CollectorAssignedPayload{
				User: idd.String(),
			})
			collectorAssigned = &Event{
				EventName: COLLECTOR_ASSIGNED,
				Data:      collectorAssignedPayload,
			}

			articleCollectedPayload, _ := types.MarshalAny(&ArticleCollectedPayload{
				Article: demoArticle,
			})

			articleCollected = &Event{
				EventName: ARTICLE_COLLECTED,
				Data:      articleCollectedPayload,
			}

		})

		It("can collect an article", func() {
			Given(collectCreated, collectorAssigned).
				When(COLLECT_ARTICLE, &CollectArticlePayload{demoArticle}).
				Then(ARTICLE_COLLECTED, Fields{
				"Article": Equal(demoArticle),
			})
		})

		It("can collect another article and completes collection", func() {
			Given(collectCreated, collectorAssigned, articleCollected).
				When(COLLECT_ARTICLE, &CollectArticlePayload{demoArticle1}).
				ThenMany(map[string]Fields{
				ARTICLE_COLLECTED: {
					"Article": Equal(demoArticle1),
				},
				COLLECT_COMPLETED: nil,
			})
		})

		It("fails on unknown article", func() {
			idd, _ := uuid.NewV4()
			Given(collectCreated, collectorAssigned).
				When(COLLECT_ARTICLE, &CollectArticlePayload{idd.String()}).
				ThenFail()
		})
	})
})
