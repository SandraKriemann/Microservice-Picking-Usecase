import com.google.protobuf.Empty;
import io.grpc.stub.StreamObserver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import warehouse.Event;
import warehouse.EventStoreGrpc;

public class EventStoreWrapper {
    private EventStoreGrpc.EventStoreStub stub;
    private static final Logger LOGGER = LoggerFactory.getLogger("EventStore");


    public EventStoreWrapper(EventStoreGrpc.EventStoreStub stub) {
        this.stub = stub;
    }

    void store(Event event){
        StreamObserver<Empty> streamObserver = new StreamObserver<Empty>() {
            @Override
            public void onNext(Empty empty) {
                LOGGER.info("Got next Event: " + empty.toString());
            }

            @Override
            public void onError(Throwable throwable) {
                LOGGER.error("Error in Event:" + throwable);
            }

            @Override
            public void onCompleted() {
                LOGGER.info("Completed Event.");
            }
        };

        stub.store(event, streamObserver);
    }
}
