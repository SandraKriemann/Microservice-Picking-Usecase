﻿namespace PickingProcessor
{
    using System;
    using System.Reactive.Linq;
    using Google.Protobuf.WellKnownTypes;
    using Grpc.Core;
    using STAN.Client;
    using Vlek;

    internal class EventStream : IEventStream, IDisposable
    {
        private readonly IStanConnection _connection;
        private readonly EventStore.EventStoreClient _eventStore;

        public string Actor { get; set; }

        public EventStream(string natsUrl, string cluster, string client, string eventStoreUrl)
        {
            var cf = new StanConnectionFactory();
            var opts = StanOptions.GetDefaultOptions();
            opts.NatsURL = natsUrl;

            _connection = cf.CreateConnection(cluster, client, opts);

            var channel = new Channel(eventStoreUrl, ChannelCredentials.Insecure);
            _eventStore = new EventStore.EventStoreClient(channel);
        }

        public IObservable<Event> Events(string eventName)
        {
            return Observable.Create<Event>(observer => _connection.Subscribe(eventName,
                (_, args) => observer.OnNext(Event.Parser.ParseFrom(args.Message.Data))));
        }

        public void Store(Event evnt)
        {
            evnt.Timestamp = Timestamp.FromDateTimeOffset(DateTimeOffset.Now);
            evnt.Actor = Actor;

            _eventStore.Store(evnt);
        }

        public void Dispose() => _connection?.Dispose();
    }
}