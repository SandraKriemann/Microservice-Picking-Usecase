﻿using System;
using Vlek;

namespace PickingProcessor

{
    public interface IEventStream
    {
        IObservable<Event> Events(string eventName);

        void Store(Event evnt);
    }
}